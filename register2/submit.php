<?php

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error=0;
$error_text="";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
//1
if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check))
{
	$error=1;
	$error_text="empty field";
}
//2
if(strlen($lastname)<3 || strlen($lastname)>20 || strlen($firstname)<3 || strlen($firstname)>20)
{
	$error=1;
	$error_text="last/first name too short/long";
}
if(strlen($question)<15)
{
	$error=1;
	$error_text="question too short";
}

//3
if(!ctype_alpha($lastname) || !ctype_alpha($firstname))
{
	$error=1;
	$error_text="the first/last name has a number";
}

//4
if((!is_numeric($phone)) || strlen($phone)!=10)
{
	$error = 1;
	$error_text = "Phone number is not valid";
}

//5
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  $error_text = "Invalid email format";
	$error=1;
}

//6
if(strlen($cnp)!=13 || $cnp/pow(10,13)>7)
{
	$error=1;
	$error_text="cnp inccorect";
}

//9
if($captcha_inserted!=$captcha_generated)
{
	$error=1;
	$error_text="wrong captcha";
}

//8
$date = date('m/d/Y h:i:s a', time());
$sec=$date-$birth;
if($sec<31536000)
{
	$error=1;
	$error_text="to young";
}



try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
if($error==0)
{
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";


}else{
    echo "Succes";
}}
else echo $error_text;
